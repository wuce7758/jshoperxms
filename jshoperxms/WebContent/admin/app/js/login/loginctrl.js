define(['./module'],function(loginmodule){
	'use strict';
	
	loginmodule.directive('social',function(){
		return{
			restrict:'E',
			templateUrl:'app/tpls/social_networks_tpl.html',
			replace:true
		}
	});
	loginmodule.controller('dologinctrl',['$scope','$http','$location','$window',function($scope,$http,$location,$window){
		$scope.login=function(){
			$http({
				method: 'POST',
				url: '../mall/user/login.action',
				data:{
					'username':$scope.username,
					'password':$scope.password
				}
			}).
	    	success(function(data, status, headers, config) {
	    		if(data.sucflag){
	    			if($window.localStorage){
	    				localStorage.setItem("x-session-token",data.token);
	    				localStorage.setItem('basicuser',angular.toJson(data.bean));
	    			}
	    			$window.location.href='index#/index';
	    		}else{
	    			$scope.msg=data.msg;
	    		}
	    	}).
	    	error(function(data, status, headers, config) {
	    		$window.location.href='404.html';
	    	});
		}
	}]);
	loginmodule.directive('submittologin',function(){
		return{
			restrict:'AE',
			link:function(scope,element,attr){
				element.bind('click',function(){
					scope.msg='';
					scope.login();
				});
			}
		}
	});
});

