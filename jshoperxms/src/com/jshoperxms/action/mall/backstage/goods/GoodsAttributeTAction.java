package com.jshoperxms.action.mall.backstage.goods;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.InterceptorRefs;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import com.jshoperxms.action.mall.backstage.base.BaseTAction;
import com.jshoperxms.action.mall.backstage.vo.GoodsAttributeListVo;
import com.jshoperxms.action.utils.BaseTools;
import com.jshoperxms.action.utils.enums.BaseEnums.DataUsingState;
import com.jshoperxms.action.utils.enums.GoodsAttrEnums.AttrIsSameToLink;
import com.jshoperxms.action.utils.enums.GoodsAttrEnums.AttrIsSearch;
import com.jshoperxms.action.utils.enums.GoodsAttrEnums.AttrType;
import com.jshoperxms.action.utils.json.GsonJson;
import com.jshoperxms.action.utils.statickey.StaticKey;
import com.jshoperxms.entity.GoodsAttributeT;
import com.jshoperxms.service.GoodsAttributeTService;
import com.jshoperxms.service.impl.Serial;
import com.opensymphony.xwork2.ActionContext;

@Namespace("/mall/goods/goodsattribute")
@ParentPackage("jshoperxms")
@InterceptorRefs({ @InterceptorRef("mallstack") })
public class GoodsAttributeTAction extends BaseTAction{
	@Resource
	private GoodsAttributeTService goodsAttributeTService;
	
	
	
	/**
	 * 保存商品属性
	 * @return
	 */
	@Action(value = "save", results = { @Result(name = "json", type = "json") })
	public String saveGoodsAttributeT(){
		GoodsAttributeT ga=new GoodsAttributeT();
		ga.setGoodsattributeid(this.getSerial().Serialid(Serial.GOODSATTRIBUTE));
		ga.setGoodsattributename(this.getGoodsattributename());
		ga.setGoodsTypeId(this.getGoodsTypeId());
		ga.setGoodsTypeName(this.getGoodsTypeName());
		ga.setCreatetime(BaseTools.getSystemTime());
		ga.setStatus(DataUsingState.USING.getState());
		ga.setCreatorid(BaseTools.getAdminCreateId());
		ga.setAttributeType(AttrType.TEXT.getState());
		ga.setAttributelist(this.getAttributelist());
		ga.setSort(this.getSort());
		ga.setAttributeIndex(ga.getGoodsattributeid());
		ga.setIssearch(this.getIssearch());
		ga.setIssametolink(this.getIssametolink());
		ga.setUpdatetime(BaseTools.getSystemTime());
		ga.setWeight(this.getWeight());
		ga.setVersiont(1);
		this.goodsAttributeTService.save(ga);
		this.setSucflag(true);
		return JSON;
	}

	/**
	 * 查询所有商品属性
	 * @return
	 */
	@Action(value = "findByPage", results = { @Result(name = "json", type = "json") })
	public String findGoodsAttributeTByPage(){
		Map<String,Object>params=ActionContext.getContext().getParameters();
		findDefaultAllGoodsAttributeT();
		return JSON;
	}
	
	
	
	
	
	private void findDefaultAllGoodsAttributeT() {
		int currentPage=1;
		if(this.getStart()!=0){
			currentPage=this.getStart()/this.getLength()==1?2:this.getStart()/this.getLength()+1;
		}
		int lineSize = this.getLength();
		Criterion criterion=Restrictions.ne("status", DataUsingState.DEL.getState());
		recordsFiltered=recordsTotal=this.goodsAttributeTService.count(GoodsAttributeT.class,criterion).intValue();
		List<GoodsAttributeT>list=this.goodsAttributeTService.findByCriteriaByPage(GoodsAttributeT.class,criterion, Order.desc("updatetime"), currentPage, lineSize);
		this.processGoodsAttributeTList(list);
	}

	private void processGoodsAttributeTList(List<GoodsAttributeT> list) {
		for(Iterator<GoodsAttributeT>it=list.iterator();it.hasNext();){
			GoodsAttributeT ga=it.next();
			Map<String,Object>cellMap=new HashMap<String, Object>();
			cellMap.put("id", ga.getGoodsattributeid());
			cellMap.put("goodsattributename", ga.getGoodsattributename());
			cellMap.put("goodsTypeName", ga.getGoodsTypeName());
			List<GoodsAttributeListVo>galist=GsonJson.parseJsonToData(ga.getAttributelist(), GoodsAttributeListVo.class);
			List<String>gst=new ArrayList<String>();
			for(GoodsAttributeListVo g:galist){
				gst.add(g.getAttrname());
			}
			cellMap.put("attributelist", StringUtils.join(gst.toArray(), StaticKey.SPLITDOT));
			cellMap.put("attributeType", AttrType.getName(ga.getAttributeType()));
			cellMap.put("issearch", AttrIsSearch.getName(ga.getIssearch()));
			cellMap.put("issametolink", AttrIsSameToLink.getName(ga.getIssametolink()));
			cellMap.put("weight", ga.getWeight());
			cellMap.put("sort", ga.getSort());
			cellMap.put("status", DataUsingState.getName(ga.getStatus()));
			cellMap.put("updatetime", BaseTools.formateDbDate(ga.getUpdatetime()));
			cellMap.put("version", ga.getVersiont());
			data.add(cellMap);
		}
	}

	/**
	 * 根据商品属性id查询内容
	 * @return
	 */
	@Action(value = "find", results = { @Result(name = "json", type = "json") })
	public String findOneGoodsAttributeT(){
		if(StringUtils.isBlank(this.getGoodsattributeid())){
			return JSON;
		}
		GoodsAttributeT ga=this.goodsAttributeTService.findByPK(GoodsAttributeT.class, this.getGoodsattributeid());
		if(ga!=null){
			bean=ga;
			this.setSucflag(true);
		}
		return JSON;
	}
	
	
	
	/**
	 * 更新商品属性
	 * @return
	 */
	@Action(value = "update", results = { @Result(name = "json", type = "json") })
	public String updateGoodsAttributeT(){
		if(StringUtils.isBlank(this.getGoodsattributeid())){
			return JSON;
		}
		GoodsAttributeT ga=this.goodsAttributeTService.findByPK(GoodsAttributeT.class, this.getGoodsattributeid());
		ga.setGoodsattributename(this.getGoodsattributename());
		ga.setGoodsTypeId(this.getGoodsTypeId());
		ga.setGoodsTypeName(this.getGoodsTypeName());
		ga.setStatus(this.getStatus());
		ga.setAttributelist(this.getAttributelist());
		ga.setSort(this.getSort());
		ga.setIssearch(this.getIssearch());
		ga.setIssametolink(this.getIssametolink());
		ga.setUpdatetime(BaseTools.getSystemTime());
		ga.setWeight(this.getWeight());
		ga.setVersiont(ga.getVersiont()+1);
		this.goodsAttributeTService.update(ga);
		this.setSucflag(true);
		return JSON;
	}
	

	/**
	 * 删除商品属性
	 * @return
	 */
	@Action(value = "del", results = { @Result(name = "json", type = "json") })
	public String delGoodsAttributeT(){
		String ids[]=StringUtils.split(this.getIds(), StaticKey.SPLITDOT);
		for(String s:ids){
			GoodsAttributeT goodsAttributeT=this.goodsAttributeTService.findByPK(GoodsAttributeT.class, s);
			if(goodsAttributeT!=null){
				goodsAttributeT.setStatus(DataUsingState.DEL.getState());
				goodsAttributeT.setUpdatetime(BaseTools.getSystemTime());
				this.goodsAttributeTService.update(goodsAttributeT);
			}
		}
		this.setSucflag(true);
		return JSON;
	}
	
	

	/**
	 * 批量删除的内容串
	 */
	private String ids;

	private int weight;
	private String status;
	private String goodsattributeid;

	private String attributeIndex;

	private String attributeType;

	private String attributelist;

	private String goodsTypeId;

	private String goodsTypeName;

	private String goodsattributename;

	private String issametolink;

	private String issearch;

	private String sort;
	
	private GoodsAttributeT bean;
	
	private List<Map<String, Object>> data = new ArrayList<Map<String, Object>>();
	private int recordsTotal = 0;
	private int recordsFiltered=0;
	/**
	 * 表格控件请求次数
	 */
	private int draw;
	/**
	 * 请求第几页
	 */
	private int start;
	/**
	 * 请求几条
	 */
	private int length;
	private boolean sucflag;

	
	
	
	public String getStatus() {
		return status;
	}


	public void setStatus(String status) {
		this.status = status;
	}


	public int getWeight() {
		return weight;
	}

	public void setWeight(int weight) {
		this.weight = weight;
	}

	public String getIds() {
		return ids;
	}

	public void setIds(String ids) {
		this.ids = ids;
	}

	public GoodsAttributeT getBean() {
		return bean;
	}

	public void setBean(GoodsAttributeT bean) {
		this.bean = bean;
	}

	public List<Map<String, Object>> getData() {
		return data;
	}

	public void setData(List<Map<String, Object>> data) {
		this.data = data;
	}

	public int getRecordsTotal() {
		return recordsTotal;
	}

	public void setRecordsTotal(int recordsTotal) {
		this.recordsTotal = recordsTotal;
	}

	public int getRecordsFiltered() {
		return recordsFiltered;
	}

	public void setRecordsFiltered(int recordsFiltered) {
		this.recordsFiltered = recordsFiltered;
	}

	public int getDraw() {
		return draw;
	}

	public void setDraw(int draw) {
		this.draw = draw;
	}

	public int getStart() {
		return start;
	}

	public void setStart(int start) {
		this.start = start;
	}

	public int getLength() {
		return length;
	}

	public void setLength(int length) {
		this.length = length;
	}

	public boolean isSucflag() {
		return sucflag;
	}

	public void setSucflag(boolean sucflag) {
		this.sucflag = sucflag;
	}


	public String getGoodsattributeid() {
		return goodsattributeid;
	}

	public void setGoodsattributeid(String goodsattributeid) {
		this.goodsattributeid = goodsattributeid;
	}

	public String getAttributeIndex() {
		return attributeIndex;
	}

	public void setAttributeIndex(String attributeIndex) {
		this.attributeIndex = attributeIndex;
	}

	public String getAttributeType() {
		return attributeType;
	}

	public void setAttributeType(String attributeType) {
		this.attributeType = attributeType;
	}

	public String getAttributelist() {
		return attributelist;
	}

	public void setAttributelist(String attributelist) {
		this.attributelist = attributelist;
	}

	public String getGoodsTypeId() {
		return goodsTypeId;
	}

	public void setGoodsTypeId(String goodsTypeId) {
		this.goodsTypeId = goodsTypeId;
	}

	public String getGoodsTypeName() {
		return goodsTypeName;
	}

	public void setGoodsTypeName(String goodsTypeName) {
		this.goodsTypeName = goodsTypeName;
	}

	public String getGoodsattributename() {
		return goodsattributename;
	}

	public void setGoodsattributename(String goodsattributename) {
		this.goodsattributename = goodsattributename;
	}

	public String getIssametolink() {
		return issametolink;
	}

	public void setIssametolink(String issametolink) {
		this.issametolink = issametolink;
	}

	public String getIssearch() {
		return issearch;
	}

	public void setIssearch(String issearch) {
		this.issearch = issearch;
	}

	public String getSort() {
		return sort;
	}

	public void setSort(String sort) {
		this.sort = sort;
	}

}
