package com.jshoperxms.service;


import java.util.List;

import com.jshoperxms.entity.CartT;
import com.jshoperxms.entity.OrderInvoiceT;
import com.jshoperxms.entity.OrderT;
import com.jshoperxms.entity.ShippingAddressT;

public interface OrderTService extends BaseTService<OrderT>{


	/**
	 * 初始化普通订单需要的信息并增加到数据表
	 * @param ordert
	 * @param sAddressT
	 */
	public void saveNormalOrderNeedInfoBack(OrderT ordert,ShippingAddressT sAddressT,List<CartT>cartLists,OrderInvoiceT oit);
	
}
