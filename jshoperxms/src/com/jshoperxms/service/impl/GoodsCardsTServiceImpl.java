package com.jshoperxms.service.impl;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.jshoperxms.entity.GoodsCardsT;
import com.jshoperxms.service.GoodsCardsTService;
@Service("goodsCardsTService")
@Scope("prototype")
public class GoodsCardsTServiceImpl extends BaseTServiceImpl<GoodsCardsT> implements GoodsCardsTService {
	
}
