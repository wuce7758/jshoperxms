package com.jshoperxms.service.impl;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.jshoperxms.entity.PostsReplyT;
import com.jshoperxms.service.PostsReplyTService;

@Service("postsReplyTService")
@Scope("prototype")
public class PostsReplyTServiceImpl extends BaseTServiceImpl<PostsReplyT> implements PostsReplyTService {

}
