package com.jshoperxms.service.impl;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.jshoperxms.entity.MemberRechargeRecordsT;
import com.jshoperxms.service.MemberRechargeRecordsTService;

@Service("memberRechargeRecordsTService")
@Scope("prototype")
public class MemberRechargeRecordsTServiceImpl extends BaseTServiceImpl<MemberRechargeRecordsT>implements
		MemberRechargeRecordsTService {


}
