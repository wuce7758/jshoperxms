package com.jshoperxms.dao.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import com.jshoperxms.dao.TemplatesetTDao;
import com.jshoperxms.entity.TemplatesetT;


@Repository("templatesetTDao")
public class TemplatesetTDaoImpl extends BaseTDaoImpl<TemplatesetT> implements TemplatesetTDao {
	
	private static final Logger log = LoggerFactory.getLogger(TemplatesetTDaoImpl.class);
	

	
}